package artx.jazz;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class FindById {

    private static Logger LOGGER = Logger.getLogger(FindById.class.getName());
    
    public static void main(String[] args) {

    	String originPath = args[0];
    	String diffPath = args[1];
    	String targetElementId = args.length > 2 ? args[2] : "make-everything-ok-button";
    	
        Element button = findElementByAttribute(getDocument(new File(originPath)), "id", targetElementId);

        Map<String, String> attributes = getAttributesMap(button);

        if (attributes.isEmpty()) {
        	LOGGER.log(Level.INFO, "Attributes not found");
        } else {
        	Document diffDoc = getDocument(new File(diffPath));
        	LOGGER.log(Level.INFO, getDiffPath(diffDoc, attributes));
        }
    }

    private static String getDiffPath(Document diffDoc, Map<String, String> attributes) {
    	
    	Map<Element, Map<String, String>> findedNodes = new HashMap<>(attributes.size());
    	
		for (Entry<String, String> attr : attributes.entrySet()) {
			
			Element element = findElementByAttribute(diffDoc, (String) attr.getKey(), (String) attr.getValue());
			
			if (element != null) {
				
				findedNodes.put(element, getAttributesMap(element));
			}
		}
		
		Element result = null;
		int attrEqual = 0;

		for (Entry<Element, Map<String, String>> node : findedNodes.entrySet()) {
			
			int comparedAttr = compare(attributes, (Map<String, String>) node.getValue());
			
			if (comparedAttr > attrEqual) {
				attrEqual = comparedAttr;
				result = (Element) node.getKey();
			}
		}
		
		return getElementPath(result);
	}

	private static int compare(Map<String, String> a, Map<String, String> b) {

		int result = 0;
		
		for (Entry<String, String> entry : a.entrySet()) {
			
			String bValue = b.get(entry.getKey());
			
			if (bValue != null && bValue.equals(entry.getValue())) {
				result++;
			}
		}
		
		return result;
	}

	private static String getElementPath(Element element) {

		if (element == null) {
			return "Not found";
		}
		
		String result = ">" + element.getTagName() + " : " + element.getTextContent(); 
		Node node = element;
		while ((node = node.getParentNode()) != null) {
			result = ">" + node.getNodeName() + result; 
		}
		
		return result;
	}

	private static Map<String, String> getAttributesMap(Element element) {
    	
    	Map<String, String> attrsMap = new HashMap<>();
    	
    	if (element == null) {
    		return attrsMap;	
    	}
    	
    	NamedNodeMap attributes = element.getAttributes();
    	
    	for (int i = 0; i < attributes.getLength(); i++) {
        
    		Attr attr = (Attr) attributes.item(i);
    		attrsMap.put(attr.getNodeName(), attr.getNodeValue());
    	}   
    	
    	return attrsMap;
    }

	private static Element findElementByAttribute(Document doc, String attributeName, String attributeValue) {
		
		if (doc == null) {
        	return null;
        }
        
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        
        try {
        	
        	XPathExpression expr = xpath.compile("//*[@" + attributeName + "=\"" + attributeValue + "\"]");
			NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

			return nl.getLength() == 0 ? null : (Element) nl.item(0);
			
		} catch (XPathExpressionException e) {
			
			LOGGER.log(Level.SEVERE, "Error finding element with attribute {0} = {1}", new Object[] { attributeName, attributeValue });
			return null;
		}
	}

	private static Document getDocument(File htmlFile) {
		
        try {
        	
        	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        	return factory.newDocumentBuilder().parse(htmlFile);

        } catch (SAXException | IOException | ParserConfigurationException e) {
            LOGGER.log(Level.SEVERE, "Error reading {0} file", htmlFile.getAbsolutePath());
            return null;
        }
		
		
	}

}
